# Translations template for bridgedb.
# Copyright (C) 2020 'The Tor Project, Inc.'
# This file is distributed under the same license as the bridgedb project.
# 
# Translators:
# D, 2014-2015
# Emin Mastizada <emin@linux.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: 'https://trac.torproject.org/projects/tor/newticket?component=BridgeDB&keywords=bridgedb-reported,msgid&cc=isis,sysrqb&owner=isis'\n"
"POT-Creation-Date: 2020-05-14 14:21-0700\n"
"PO-Revision-Date: 2020-05-15 08:24+0000\n"
"Last-Translator: Transifex Bot <>\n"
"Language-Team: Azerbaijani (http://www.transifex.com/otf/torproject/language/az/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"
"Language: az\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. -*- coding: utf-8 ; test-case-name: bridgedb.test.test_https_server -*-
#. This file is part of BridgeDB, a Tor bridge distribution system.
#. :authors: please see included AUTHORS file
#. :copyright: (c) 2007-2017, The Tor Project, Inc.
#. (c) 2013-2017, Isis Lovecruft
#. :license: see LICENSE for licensing information
#. : The path to the HTTPS distributor's web templates.  (Should be the
#. : "templates" directory in the same directory as this file.)
#. Setting `filesystem_checks` to False is recommended for production servers,
#. due to potential speed increases. This means that the atimes of the Mako
#. template files aren't rechecked every time the template is requested
#. (otherwise, if they are checked, and the atime is newer, the template is
#. recompiled). `collection_size` sets the number of compiled templates which
#. are cached before the least recently used ones are removed. See:
#. http://docs.makotemplates.org/en/latest/usage.html#using-templatelookup
#. : A list of supported language tuples. Use getSortedLangList() to read this
#. variable.
#. We use our metrics singleton to keep track of BridgeDB metrics such as
#. "number of failed HTTPS bridge requests."
#. Convert all key/value pairs from bytes to str.
#. TRANSLATORS: Please DO NOT translate the following words and/or phrases in
#. any string (regardless of capitalization and/or punctuation):
#. "BridgeDB"
#. "pluggable transport"
#. "pluggable transports"
#. "obfs4"
#. "Tor"
#. "Tor Browser"
#: bridgedb/distributors/https/server.py:154
msgid "Sorry! Something went wrong with your request."
msgstr "Üzr istəyirik! Tələbinlə bağlı nə isə səhv oldu."

#: bridgedb/distributors/https/templates/base.html:42
msgid "Language"
msgstr "Dil"

#: bridgedb/distributors/https/templates/base.html:94
msgid "Report a Bug"
msgstr "Problemi Xəbər Ver"

#: bridgedb/distributors/https/templates/base.html:97
msgid "Source Code"
msgstr "Mənbə Kodu"

#: bridgedb/distributors/https/templates/base.html:100
msgid "Changelog"
msgstr "Dəyişikliklər"

#: bridgedb/distributors/https/templates/bridges.html:35
msgid "Select All"
msgstr "Hamısını Seç"

#: bridgedb/distributors/https/templates/bridges.html:40
msgid "Show QRCode"
msgstr "QR Kodu Göstər"

#: bridgedb/distributors/https/templates/bridges.html:52
msgid "QRCode for your bridge lines"
msgstr "Körpü sətirləriniz üçün QR Kod"

#: bridgedb/distributors/https/templates/bridges.html:63
msgid "It seems there was an error getting your QRCode."
msgstr "Görünür QR Kodunuzu almada səhv yarandı."

#: bridgedb/distributors/https/templates/bridges.html:68
msgid ""
"This QRCode contains your bridge lines. Scan it with a QRCode reader to copy"
" your bridge lines onto mobile and other devices."
msgstr "Bu QR Kodunda körpü sətirləriniz var. Bunu QR Kod oxuyucularla gözdən keçirin və körpü sətirlərinizi mobil və digər cihazlara köçürün."

#: bridgedb/distributors/https/templates/bridges.html:110
msgid "BridgeDB encountered an error."
msgstr ""

#: bridgedb/distributors/https/templates/bridges.html:116
msgid "There currently aren't any bridges available..."
msgstr "Hazırda mümkün körpü yoxdur..."

#: bridgedb/distributors/https/templates/bridges.html:118
#: bridgedb/distributors/https/templates/bridges.html:122
#, python-format
msgid ""
" Perhaps you should try %s going back %s and choosing a different bridge "
"type!"
msgstr "Çox güman ki, sən %s geri qayıdıb %s başqa körpü növü seçməli idin!"

#: bridgedb/distributors/https/templates/index.html:11
#, python-format
msgid "Step %s1%s"
msgstr "%s1-ci%s Addım"

#: bridgedb/distributors/https/templates/index.html:13
#, python-format
msgid "Download %s Tor Browser %s"
msgstr "%s Tor Səyyahını %s Endir"

#: bridgedb/distributors/https/templates/index.html:25
#, python-format
msgid "Step %s2%s"
msgstr "%s2-ci%s Addım"

#: bridgedb/distributors/https/templates/index.html:28
#: bridgedb/distributors/https/templates/index.html:30
#, python-format
msgid "Get %s bridges %s"
msgstr "%s Körpü %s Əldə Et"

#: bridgedb/distributors/https/templates/index.html:40
#, python-format
msgid "Step %s3%s"
msgstr "%s3-cü%s Addım"

#: bridgedb/distributors/https/templates/index.html:43
#: bridgedb/distributors/https/templates/index.html:47
#, python-format
msgid "Now %s add the bridges to Tor Browser %s"
msgstr "İndi %s körpüləri Tor Səyyahına əlavə et %s"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. (These are used to insert HTML5 underlining tags, to mark accesskeys
#. for disabled users.)
#: bridgedb/distributors/https/templates/options.html:42
#, python-format
msgid "%sJ%sust give me bridges!"
msgstr "%sS%sadəcə mənə körpüləri ver!"

#: bridgedb/distributors/https/templates/options.html:55
msgid "Advanced Options"
msgstr "Təkmilləşdirilmiş Seçimlər"

#: bridgedb/distributors/https/templates/options.html:93
msgid "No"
msgstr "Xeyr"

#: bridgedb/distributors/https/templates/options.html:94
msgid "none"
msgstr "heç biri"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. TRANSLATORS: Translate "Yes!" as in "Yes! I do need IPv6 addresses."
#: bridgedb/distributors/https/templates/options.html:131
#, python-format
msgid "%sY%ses!"
msgstr "%sB%səli!"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. TRANSLATORS: Please do NOT translate the word "bridge"!
#: bridgedb/distributors/https/templates/options.html:154
#, python-format
msgid "%sG%set Bridges"
msgstr "%sK%sörpü Əldə Et"

#: bridgedb/strings.py:33
msgid "[This is an automated email.]"
msgstr ""

#: bridgedb/strings.py:35
msgid "Here are your bridges:"
msgstr "Bu da körpüləriniz:"

#: bridgedb/strings.py:37
#, python-format
msgid ""
"You have exceeded the rate limit. Please slow down! The minimum time between\n"
"emails is %s hours. All further emails during this time period will be ignored."
msgstr "Səs limitini keçmisən. Lütfən yavaşla! Emaillər arası minimal vaxt\n%s saatdır. Bu zaman kəsiyində daxil edilən diər emaillər ləğv ediləcəkdir."

#: bridgedb/strings.py:40
msgid ""
"If these bridges are not what you need, reply to this email with one of\n"
"the following commands in the message body:"
msgstr ""

#. TRANSLATORS: Please DO NOT translate "BridgeDB".
#. TRANSLATORS: Please DO NOT translate "Pluggable Transports".
#. TRANSLATORS: Please DO NOT translate "Tor".
#. TRANSLATORS: Please DO NOT translate "Tor Network".
#: bridgedb/strings.py:50
#, python-format
msgid ""
"BridgeDB can provide bridges with several %stypes of Pluggable Transports%s,\n"
"which can help obfuscate your connections to the Tor Network, making it more\n"
"difficult for anyone watching your internet traffic to determine that you are\n"
"using Tor.\n"
"\n"
msgstr "BridgeDB müxtəlif %s növ Pluggable Transport %s vasitəsilə körpülər müəyyən \nedə bilər, bu sənin internet trafikini yoxlamaq istəyən hər hansı şəxs üçün \nəlaqələrini Tor Network ilə kölgələyərək sənin Tor istifadə etdiyini müəyyən \netməsinə daha çox mane olacaqdır.\n\n"

#. TRANSLATORS: Please DO NOT translate "Pluggable Transports".
#: bridgedb/strings.py:57
msgid ""
"Some bridges with IPv6 addresses are also available, though some Pluggable\n"
"Transports aren't IPv6 compatible.\n"
"\n"
msgstr "Pluggable Transports vasitəsilə mövcud olan IPv6 ünvanlı bir çox \nkörpülər IPv6 ilə uyğun deyillər.\n\n"

#. TRANSLATORS: Please DO NOT translate "BridgeDB".
#. TRANSLATORS: The phrase "plain-ol'-vanilla" means "plain, boring,
#. regular, or unexciting". Like vanilla ice cream. It refers to bridges
#. which do not have Pluggable Transports, and only speak the regular,
#. boring Tor protocol. Translate it as you see fit. Have fun with it.
#: bridgedb/strings.py:66
#, python-format
msgid ""
"Additionally, BridgeDB has plenty of plain-ol'-vanilla bridges %s without any\n"
"Pluggable Transports %s which maybe doesn't sound as cool, but they can still\n"
"help to circumvent internet censorship in many cases.\n"
"\n"
msgstr "Bundan əlavə BridgeDB Pluggable Transports %s olmayan bir sıra darıxdırıcı \nkörpülər %s də vardır, amma onlar yenə də internet senzurasından \nsovuşmağı bacarırlar.\n\n"

#: bridgedb/strings.py:78 bridgedb/test/test_https.py:356
msgid "What are bridges?"
msgstr "Körpülər nədir?"

#: bridgedb/strings.py:79
#, python-format
msgid "%s Bridges %s are Tor relays that help you circumvent censorship."
msgstr "%s Körpülər %s Tor-un keçidi olub sənə senzuradan sovuşmağa kömək edir."

#: bridgedb/strings.py:84
msgid "I need an alternative way of getting bridges!"
msgstr "Körpülərin əldə edilməsinin alternativ yollarını axtarıram!"

#. TRANSLATORS: Please DO NOT translate "get transport obfs4".
#: bridgedb/strings.py:86
#, python-format
msgid ""
"Another way to get bridges is to send an email to %s. Leave the email subject\n"
"empty and write \"get transport obfs4\" in the email's message body. Please note\n"
"that you must send the email using an address from one of the following email\n"
"providers: %s or %s."
msgstr ""

#: bridgedb/strings.py:94
msgid "My bridges don't work! I need help!"
msgstr "Körpülərim işlərmir! Köməyə ehtiyacım var!"

#. TRANSLATORS: Please DO NOT translate "Tor Browser".
#. TRANSLATORS: The two '%s' are substituted with "Tor Browser Manual" and
#. "Support Portal", respectively.
#: bridgedb/strings.py:98
#, python-format
msgid ""
"If your Tor Browser cannot connect, please take a look at the %s and our %s."
msgstr ""

#: bridgedb/strings.py:102
msgid "Here are your bridge lines:"
msgstr "Sənin körpü sətirlərin:"

#: bridgedb/strings.py:103
msgid "Get Bridges!"
msgstr "Körpü Əldə Et!"

#: bridgedb/strings.py:107
msgid "Bridge distribution mechanisms"
msgstr ""

#. TRANSLATORS: Please DO NOT translate "BridgeDB", "HTTPS", and "Moat".
#: bridgedb/strings.py:109
#, python-format
msgid ""
"BridgeDB implements four mechanisms to distribute bridges: \"HTTPS\", \"Moat\",\n"
"\"Email\", and \"Reserved\".  Bridges that are not distributed over BridgeDB use\n"
"the pseudo-mechanism \"None\".  The following list briefly explains how these\n"
"mechanisms work and our %sBridgeDB metrics%s visualize how popular each of the\n"
"mechanisms is."
msgstr ""

#: bridgedb/strings.py:115
#, python-format
msgid ""
"The \"HTTPS\" distribution mechanism hands out bridges over this website.  To get\n"
"bridges, go to %sbridges.torproject.org%s, select your preferred options, and\n"
"solve the subsequent CAPTCHA."
msgstr ""

#: bridgedb/strings.py:119
#, python-format
msgid ""
"The \"Moat\" distribution mechanism is part of Tor Browser, allowing users to\n"
"request bridges from inside their Tor Browser settings.  To get bridges, go to\n"
"your Tor Browser's %sTor settings%s, click on \"request a new bridge\", solve the\n"
"subsequent CAPTCHA, and Tor Browser will automatically add your new\n"
"bridges."
msgstr ""

#: bridgedb/strings.py:125
#, python-format
msgid ""
"Users can request bridges from the \"Email\" distribution mechanism by sending an\n"
"email to %sbridges@torproject.org%s and writing \"get transport obfs4\" in the\n"
"email body."
msgstr ""

#: bridgedb/strings.py:129
msgid "Reserved"
msgstr ""

#: bridgedb/strings.py:130
#, python-format
msgid ""
"BridgeDB maintains a small number of bridges that are not distributed\n"
"automatically.  Instead, we reserve these bridges for manual distribution and\n"
"hand them out to NGOs and other organizations and individuals that need\n"
"bridges.  Bridges that are distributed over the \"Reserved\" mechanism may not\n"
"see users for a long time.  Note that the \"Reserved\" distribution mechanism is\n"
"called \"Unallocated\" in %sbridge pool assignment%s files."
msgstr ""

#: bridgedb/strings.py:137
msgid "None"
msgstr "Heç biri"

#: bridgedb/strings.py:138
msgid ""
"Bridges whose distribution mechanism is \"None\" are not distributed by BridgeDB.\n"
"It is the bridge operator's responsibility to distribute their bridges to\n"
"users.  Note that on Relay Search, a freshly set up bridge's distribution\n"
"mechanism says \"None\" for up to approximately one day.  Be a bit patient, and\n"
"it will then change to the bridge's actual distribution mechanism.\n"
msgstr ""

#: bridgedb/strings.py:148
msgid "Please select options for bridge type:"
msgstr "Lütfən, körpü növləri üçün seçimləri müəyyənləşdir:"

#: bridgedb/strings.py:149
msgid "Do you need IPv6 addresses?"
msgstr "IPv6 ünvanlara ehtiyacın var?"

#: bridgedb/strings.py:150
#, python-format
msgid "Do you need a %s?"
msgstr "%s ehtiyacın var?"

#: bridgedb/strings.py:154
msgid "Your browser is not displaying images properly."
msgstr "Sənin brauzerin şəkilləri göstərə bilmir."

#: bridgedb/strings.py:155
msgid "Enter the characters from the image above..."
msgstr "Yuxarıdakı şəkildən işarələri daxil et..."

#: bridgedb/strings.py:159
msgid "How to start using your bridges"
msgstr "Körpüləri necə istifadə edəcəksən"

#. TRANSLATORS: Please DO NOT translate "Tor Browser".
#: bridgedb/strings.py:161
#, python-format
msgid ""
" First, you need to %sdownload Tor Browser%s. Our Tor Browser User\n"
" Manual explains how you can add your bridges to Tor Browser. If you are\n"
" using Windows, Linux, or OS X, %sclick here%s to learn more. If you\n"
" are using Android, %sclick here%s."
msgstr ""

#: bridgedb/strings.py:166
msgid ""
"Add these bridges to your Tor Browser by opening your browser\n"
"preferences, clicking on \"Tor\", and then adding them to the \"Provide a\n"
"bridge\" field."
msgstr ""

#: bridgedb/strings.py:173
msgid "(Request unobfuscated Tor bridges.)"
msgstr ""

#: bridgedb/strings.py:174
msgid "(Request IPv6 bridges.)"
msgstr ""

#: bridgedb/strings.py:175
msgid "(Request obfs4 obfuscated bridges.)"
msgstr ""
